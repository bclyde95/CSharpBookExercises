﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inputs;

namespace Chapter7Exercises
{
    class Program
    {
        static void PrintMatrix (int[,] mat)
        {
            for (int i = 0; i < mat.GetLength(0); i++)
            {
                for (int j = 0; j < mat.GetLength(1); j++)
                {
                    if (mat[i, j] > 9 && mat[i,j] < 100)
                    {
                        Console.Write("|  {0} |", mat[i, j]);
                    }
                    else if(mat[i, j] > 99)
                    {
                        Console.Write("| {0} |", mat[i, j]);
                    }
                    else
                    {
                        Console.Write("|  {0}  |", mat[i, j]);
                    }
                }
                Console.WriteLine();
            }
        }

        static int[,] InputMatrix ()
        {
            #region # of Rows

            Console.Write("Enter the number of rows in your matrix: ");
            int rows = InputHandling.IntParse();
            Console.WriteLine();

            #endregion

            #region # of Columns

            Console.Write("Enter the number of columns in your matrix: ");
            int cols = InputHandling.IntParse();
            Console.WriteLine();

            #endregion

            int[,] matrix = new int[rows, cols];

            #region Fill in the matrix

            Console.WriteLine("Enter the cells of the matrix");

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    Console.Write("Matrix[{0}, {1}] = ", row, col);
                    matrix[row, col] = InputHandling.IntParse();
                }
            }

            #endregion

            return matrix;
        }

        //static void Main(string[] args)
        //{
        //    int[,] mat = InputMatrix();
        //    PrintMatrix(mat);
        //}
    }
}
