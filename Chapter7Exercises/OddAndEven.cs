﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter7Exercises
{
    class OddAndEven
    {
        static string Program(string s)
        {
            string one = "";
            string two = "";
            for (int i = 0; i < s.Length; i += 2)
            {
                one += s[i];
            }
            for (int j = 1; j < s.Length; j += 2)
            {
                two += s[j];
            }
            return string.Format("{0} {1}", one, two);
        }

        //static void Main (string[] args)
        //{
        //    Console.WriteLine(Program("Cindy"));
        //}
    }
}
