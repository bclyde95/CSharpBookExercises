﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Inputs
{
    class InputHandling
    {
        public static double DoubleParse()
        {
            string input = "";
            double value;
            double output;
            while (true)
            {
                try
                {
                    input = Console.ReadLine();
                }
                catch
                {
                    Console.WriteLine("An error has occurred with the input");
                }

                if (double.TryParse(input, out value))
                {
                    output = double.Parse(input);
                    break;
                }
                else
                {
                    Console.WriteLine("Please enter a valid input");
                }
            }
            return output;
        }

        public static int IntParse()
        {
            string input = "";
            int value;
            int output;
            while (true)
            {
                try
                {
                    input = Console.ReadLine();
                }
                catch
                {
                    Console.WriteLine("An error has occurred with the input");
                }

                if (int.TryParse(input, out value))
                {
                    output = int.Parse(input);
                    break;
                }
                else
                {
                    Console.WriteLine("Please enter a valid input");
                }
            }
            return output;
        }

        public static string PhoneNumberRegex()
        {
            string input;
            string output;
            while (true)
            {
                input = Console.ReadLine();
                if (Regex.IsMatch(input, "^[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$"))
                {
                    output = input;
                    break;
                }
                else
                {
                    Console.WriteLine("Enter a phone number with the correct format");
                }
            }
            return output;
        }

        public static string stringReverser (string s)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = s.Length - 1; i >= 0; i--)
            {
                sb.Append(s[i]);
            }

            return sb.ToString();
        }

        //static void Main (string[] args)
        //{
        //    string input = Console.ReadLine();
        //    double userDouble = InputDoubleParseHandling(input);
        //    Console.WriteLine(userDouble);
        //}
    }
}
