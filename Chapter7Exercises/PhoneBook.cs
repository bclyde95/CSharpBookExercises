﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Chapter7Exercises
{
    class PhoneBook
    {
        static void PhoneBookSet(int n, Dictionary<string, string> book)
        {
            for (int i = 0; i < n; i++)
            {
                string s;
                string[] array = new string[2];
                string name = array[0];
                string number;

                while (true)
                {
                    s = Console.ReadLine();
                    array = s.Split(' ');
                    name = array[0];

                    if (Regex.IsMatch(array[1], "^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$"))
                    {
                        number = s;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Enter a phone number with the correct format");
                    }
                }

                book.Add(name, number);
            }
        }

        static void PhoneBookGet(Dictionary<string, string> book)
        {
            while (true)
            {
                string s = Console.ReadLine();

                if (s == "quit")
                {
                    break;
                }

                try
                {
                    Console.WriteLine("{0}={1}", s, book[s]);
                }
                catch (KeyNotFoundException)
                {
                    Console.WriteLine("Not found");
                }
            }
        }

        //static void Main (string[] args)
        //{
        //    Dictionary<string, string> phoneBook = new Dictionary<string, string>();
        //    int num = int.Parse(Console.ReadLine());

        //    PhoneBookSet(num, phoneBook);

        //    PhoneBookGet(phoneBook);
        //}
    }
}
