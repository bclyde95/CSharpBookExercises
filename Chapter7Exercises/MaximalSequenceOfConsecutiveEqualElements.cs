﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter7Exercises
{
    class MaximalSequenceOfConsecutiveEqualElements
    {
        static void PrintList(List<int> a)
        {
            foreach (int i in a)
            {
                Console.WriteLine(i);
            }
        }

        static void ConsecutiveEqualElements (int[] a)
        {
            int bestPos = 0;
            int bestLen = 0;
            int pos = 0;
            int len = 1;
            for (int i = 0; i < a.Length - 1; i++)
            {
                if (a[i+1] == a[i])
                {
                    len++;
                    if (len > bestLen)
                    {
                        bestLen = len;
                        bestPos = pos;
                    }
                }
                else
                {
                    len = 1;
                    pos = i+1;
                }
            }
            for (int k = bestPos; k < bestPos + bestLen; k++)
            {
                Console.Write("{0}", a[k]);
            }
        }

        //static void Main (string[] args)
        //{
        //    int[] array = {1, 2, 2, 4, 4, 4, 3, 5, 2, 6, 6, 6, 3, 4, 4, 4, 2, 4, 4, 4, 4 };
        //    //Pjhsdhj(array);
        //    ConsecutiveEqualElements(array);
        //}
    }
}
