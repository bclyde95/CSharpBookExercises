﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inputs;

namespace Chapter7Exercises
{
    class EqualInputArrays
    {
        static int[] InputArray()
        {
            Console.Write("Enter the length of the array: ");
            int length = InputHandling.IntParse();

            int[] userArray = new int[length];

            Console.WriteLine("Enter the values of the array: ");
            for (int i = 0; i < length; i++)
            {
                userArray[i] = InputHandling.IntParse();
            }

            return userArray;
        }

        //static void Main (string[] args)
        //{
        //    Console.WriteLine("Enter two arrays to see if they are equal");
        //    Console.WriteLine();
        //    int[] arrayOne = InputArray();
        //    Console.WriteLine();
        //    int[] arrayTwo = InputArray();

        //    if (arrayOne.SequenceEqual(arrayTwo))
        //    {
        //        Console.WriteLine("Equal");
        //    }
        //    else
        //    {
        //        Console.WriteLine("Not equal");
        //    }
        //}
    }
}