﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter7Exercises
{
    class MaximalSequenceOfIncreasingElements
    {
        static void PrintArray (int[] a)
        {
            foreach (int i in a)
            {
                Console.Write(i);
            }
        }

        static void PrintList(List<int> a)
        {
            foreach (int i in a)
            {
                Console.Write(i);
            }
        }

        static int[] IncreasingElements (int[] a)
        {
            
            int[] P = new int[a.Length];
            int[] M = new int[a.Length + 1];

            int maxLength = 0;

            for (int i = 0; i < a.Length-1; i++)
            {
                int low = 1;
                int high = maxLength;
                decimal mid;
                decimal midDec;
                while (low <= high)
                {
                    midDec = (low + high) / 2;
                    mid = Math.Ceiling(midDec);
                    if (a[M[(Convert.ToInt32(mid))]] < a[i])
                    {
                        low = (int)mid + 1;
                    }
                    else
                    {
                        high = (int)mid - 1;
                    }
                }
                int newLength = low;
                P[i] = M[newLength - 1];
                M[newLength] = i;

                if (newLength > maxLength)
                {
                    maxLength = newLength;
                }
            }
            int[] S = new int[maxLength];
            int k = M[maxLength];
            for (int j = maxLength-1; j > 0; j--)
            {
                S[j] = a[k];
                k = P[k];
            }
            return S;
    }

        static List<int> longestSeq3(int[] seq)
        {
            int maxDist = 2;
            List<int> result = new List<int>();

            for (int i = 0; i < seq.Length; i++)
            {
                int current = seq[i];
                int dist = Math.Abs(i - current);
                if (dist >= 0 && dist <= maxDist)
                {
                    if (result.Count > 0 && current <= result.Last())
                    {
                        continue;
                    }
                    result.Add(current);
                }
            }

            return result;
        }


        //static void Main(string[] args)
        //{
        //    int[] array = { 1, 5, 2, 6, 1, 3, 7, 4, 2, 7, 8, 2, 5, 7, 6, 1, 7, 3, 8, 5, 9 };
        //    PrintArray(IncreasingElements(array));
        //    Console.WriteLine();
        //    PrintList(longestSeq3(array));
        //    Console.ReadLine();
        //}
    }
}
