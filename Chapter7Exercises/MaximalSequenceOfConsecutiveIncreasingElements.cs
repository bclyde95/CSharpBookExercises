﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter7Exercises
{
    class MaximalSequenceOfConsecutiveIncreasingElements
    {
        static void ConsecutiveIncreasingElements (int[] a)
        {
            int bestPos = 0;
            int bestLen = 0;
            int pos = 0;
            int len = 1;
            for (int i = 0; i < a.Length - 1; i++)
            {
                if (a[i + 1] == a[i] + 1)
                {
                    len++;
                    if (len > bestLen)
                    {
                        bestLen = len;
                        bestPos = pos;
                    }
                }
                else
                {
                    len = 1;
                    pos = i + 1;
                }
            }
            for (int k = bestPos; k < bestPos + bestLen; k++)
            {
                Console.Write("{0}", a[k]);
            }
        }

        //static void Main(string[] args)
        //{
        //    int[] array = { 1, 2, 3, 5, 3, 2, 6, 5, 3, 4, 5, 6, 7, 2, 7, 5, 3, 5, 4 };
        //    ConsecutiveIncreasingElements(array);
        //}
    }
}
