﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Chapter7Exercises
{
    class HourglassSum
    {
        static int MaximalHourglassSum(int[][] a)
        {
            int sum = 0;
            int bestSum = int.MinValue;
            int topRow = 0;
            int middleRow = 0;
            int bottomRow = 0;
            for (int i = 0; i < a.Length - 2; i++)
            {
                for (int j = 0; j < a[i].Length - 2; j++)
                {
                    topRow = (a[i][j] + a[i][j + 1] + a[i][j + 2]);
                    middleRow = a[i + 1][j + 1];
                    bottomRow = (a[i + 2][j] + a[i + 2][j + 1] + a[i + 2][j + 2]);
                    sum = topRow + middleRow + bottomRow;

                    if (sum > bestSum)
                    {
                        bestSum = sum;
                    }
                }
            }

            return bestSum;
        }

        static void Main(string[] args)
        {
            int[][] arr = new int[][]
            {
                new int[] { 1, 1, 1, 0, 0, 0 },
                new int[] { 0, 1, 0, 0, 0, 0 },
                new int[] { 1, 1, 1, 0, 0, 0 },
                new int[] { 0, 0, 2, 4, 4, 0 },
                new int[] { 0, 0, 0, 2, 0, 0 },
                new int[] { 0, 0, 1, 2, 4, 0 }
            };
            
            Console.WriteLine(MaximalHourglassSum(arr));
        }
    }
}
