﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inputs;

namespace Chapter7Exercises
{
    class MaximalSumOfKConsecutiveElements
    {
        static int[] InputArray ()
        {
            Console.Write("Enter the length of the array: ");
            int length = InputHandling.IntParse();

            int[] userArray = new int[length];

            Console.WriteLine("Enter the values of the array: ");
            for (int i = 0; i < length; i++)
            {
                Console.Write("Index {0}: ", i);
                userArray[i] = InputHandling.IntParse();
            }

            return userArray;
        }

        static string MaximalSumOfKIntegers (int[] a, int k)
        {
            int sum = 0;
            int bestSum = int.MinValue;
            int bestStart = int.MinValue;
            int bestEnd = int.MinValue;

            for (int start = 0, end = k - 1; end < a.Length; start++, end++)
            {
                sum = 0;
                for (int j = start; j <= end; j++)
                {
                    sum += a[j];
                    if (sum > bestSum)
                    {
                        bestSum = sum;
                        bestStart = start;
                        bestEnd = end;
                    }
                }
            }

            return string.Format("The maximal sum is {0}. Starts at {1}, and ends at {2}", bestSum, bestStart, bestEnd);
        }

        //static void Main(string[] args)
        //{
        //    #region Input

        //    int[] array = InputArray();
        //    Console.WriteLine("Enter a number for K");
        //    int K;
        //    while (true)
        //    {
        //        K = InputHandling.IntParse();
        //        if (K < array.Length)
        //        {
        //            break;
        //        }
        //        else
        //        {
        //            Console.WriteLine("'K' must be less than the length of the array");
        //        }
        //    }


        //    #endregion

        //    Console.WriteLine(MaximalSumOfKIntegers(array, K));
        //}
    }
}
