﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter7Exercises
{
    class MostFrequentlyOccuringElement
    {
        static string MostFrequent(int[] a)
        {
            List<int> visited = new List<int>();
            int len = 0;
            int bestLen = 0;
            int bestNum = 0;

            foreach (int i in a)
            {
                if (!visited.Contains(i))
                {
                    len = 0;
                    for (int j = 0; j < a.Length; j++)
                    {
                        if (i == a[j])
                        {
                            len += 1;
                        }
                    }
                    if (len > bestLen)
                    {
                        bestLen = len;
                        bestNum = i;
                    }
                    visited.Add(i);
                }
                else
                {

                }
            }
            return string.Format("The most frequently occurring element is:\n {0}, with {1} instances", bestNum, bestLen);
        }

        //static void Main (string[] args)
        //{
        //    int[] array = { 1, 1, 4, 2, 5, 6, 3, 3, 7, 4, 6, 3, 2, 3, 2, 3, 5, 3 };
        //    Console.WriteLine(MostFrequent(array));
        //}
    }
}
