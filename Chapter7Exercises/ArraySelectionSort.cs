﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inputs;

namespace Chapter7Exercises
{
    class ArraySelectionSort
    {
        static void PrintArray(int[] a)
        {
            foreach (int i in a)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
        }

        static int[] InputArray()
        {
            Console.Write("Enter the length of the array: ");
            int length = InputHandling.IntParse();

            int[] userArray = new int[length];

            Console.WriteLine("Enter the values of the array: ");
            for (int i = 0; i < length; i++)
            {
                userArray[i] = InputHandling.IntParse();
            }

            return userArray;
        }

        static int[] SelectionSort(int[] a)
        {
            int temp;
            for (int i = 0; i < a.Length - 1; i++)
            {
                for (int j = i+1; j < a.Length; j++)
                {
                    if (a[i] > a[j])
                    {
                        temp = a[i];
                        a[i] = a[j];
                        a[j] = temp;
                    }
                }
            }

            return a;
        }

        static int[] DescendingSelectionSort(int[] a)
        {
            int temp;
            for (int i = 0; i < a.Length - 1; i++)
            {
                for (int j = i + 1; j < a.Length; j++)
                {
                    if (a[i] < a[j])
                    {
                        temp = a[i];
                        a[i] = a[j];
                        a[j] = temp;
                    }
                }
            }

            return a;
        }

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Enter an array and see it sorted in the order of your choosing");
        //    Console.WriteLine();

        //    #region Input

        //    int[] array = InputArray();
        //    Console.Write("Enter 'asc' for ascending order or 'desc' for descending order: ");
        //    string order = "";
        //    while (true)
        //    {
        //        order = Console.ReadLine();
        //        if (order.ToLower() == "asc" || order.ToLower() == "desc")
        //        {
        //            break;
        //        }
        //        else
        //        {
        //            Console.WriteLine("Enter 'asc' or 'desc'");
        //        }
        //    }
        //    Console.WriteLine();

        //    #endregion

        //    //int[] array = { 6, 1, 7, 2, 4, 5, 9, 3, 8, 0 };
        //    if (order == "asc")
        //    {
        //        PrintArray(SelectionSort(array));
        //    }
        //    else if (order == "desc")
        //    {
        //        PrintArray(DescendingSelectionSort(array));
        //    }
        //}
    }
}
