﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter7Exercises
{
    class FirstInLexicographicalOrder
    {
        static void LexicographicalComparison(char[] a, char[] b)
        {
            if (a.SequenceEqual(b))
            {
                Console.WriteLine("The arrays are equal");
            }
            else
            {
                for (int i = 0; i < a.Length; i++)
                {
                    try
                    {
                        if (a[i] < b[i])
                        {
                            Console.WriteLine("Array 1 is first in lexicographical order");
                            break;
                        }
                        else if (a[i] > b[i])
                        {
                            Console.WriteLine("Array 2 is first in lexicographical order");
                            break;
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Console.WriteLine("Array 2 is first in lexicographical order");
                    }
                }
            }
        }

        //static void Main (string[] args)
        //{
        //    char[] arrayOne = { 'a', 'c', 's', 's', 'f', 'y', 'e', 'g' };
        //    char[] arrayTwo = { 'd', 'c', 's', 's', 'f', 'y', 'e' };
        //    LexicographicalComparison(arrayOne, arrayTwo);
        //}
    }
}
