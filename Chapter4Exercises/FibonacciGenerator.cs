﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter4Exercises
{
    class FibonacciGenerator
    {
        static public int fib(int n)
        {
            if (n == 0)
                return 0;
            if (n == 1)
                return 1;

            return fib(n - 1) + fib(n - 2);
        }

        //static void Main(string[] args)
        //{
        //    int n = 100;
        //    for (int counter = 0; counter < n; counter++)
        //        Console.WriteLine(fib(counter));

        //}
    }
}
