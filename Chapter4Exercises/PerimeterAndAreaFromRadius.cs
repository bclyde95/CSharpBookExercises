﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter4Exercises
{
    class PerimeterAndAreaFromRadius
    {
        static double CircumferenceFromRadius(double radius)
        {
            return 2 * Math.PI * radius;
        }

        static double AreaFromRadius (double radius)
        {
            return Math.PI * (radius * radius);
        }

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Enter the Radius of a circle to find the Circumference and Area");
        //    double userR = InputParseHandling.DoubleParse();

        //    double circumference = CircumferenceFromRadius(userR);
        //    Console.WriteLine("The Circumference of your Radius is {0}", circumference);

        //    double area = AreaFromRadius(userR);
        //    Console.WriteLine("The Area of your Radius is {0}", area);
        //}
    }
}
