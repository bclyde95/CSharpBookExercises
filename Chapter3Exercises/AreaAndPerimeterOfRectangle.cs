﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter3Exercises
{
    class AreaAndPerimeterOfRectangle
    {
        public static double Perimeter(double length, double width)
        {
            double perimeter = (length * 2) + (width * 2);
            return perimeter;
        }
        public static double Area (double length, double width)
        {
            double area = length * width;
            return area;
        }

        //static void Main(string[] args)
        //{
        //    //Get user input
        //    Console.WriteLine("Please enter the length of the rectangle");
        //    double userLength = InputParseHandling.DoubleParse();

        //    Console.WriteLine("Okay, now enter the width of the rectangle");
        //    double userWidth = InputParseHandling.DoubleParse();

        //    double perimeter = Perimeter(userLength, userWidth);
        //    double area = Area(userLength, userWidth);
        //    Console.WriteLine("The perimeter is " + perimeter);
        //    Console.WriteLine("The area is " + area);
        //}
    }
}
