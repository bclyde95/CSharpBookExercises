﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter3Exercises
{
    class PrimeNumberBool
    {
        public static bool IsPrime(int n)
        {
            int limit = (int)Math.Sqrt(n);
            
            if ((n & 1) == 0)
            {
                return (n == 2);
            }
            for (int i = 3; i <= limit; i += 2)
            {
                if ((n % i) == 0)
                {
                    return false;
                }
            }
            return true;
        }

        //static void Main (string[] args)
        //{
        //    Console.WriteLine("Check if a number is prime");
        //    int userInt = InputParseHandling.IntParse();
        //    bool isPrime = IsPrime(userInt);

        //    Console.WriteLine(isPrime);
        //}
    }
}
