﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomFuncs
{
    class InputParseHandling
    {
        public static double DoubleParse()
        {
            string input;
            double value;
            double output;
            while (true)
            {
                input = Console.ReadLine();
                if (double.TryParse(input, out value))
                {
                    output = double.Parse(input);
                    break;
                }
                else
                {
                    Console.WriteLine("Please enter a valid input");
                }
            }
            return output;
        }

        public static int IntParse()
        {
            string input;
            int value;
            int output;
            while (true)
            {
                input = Console.ReadLine();
                if (int.TryParse(input, out value))
                {
                    output = int.Parse(input);
                    break;
                }
                else
                {
                    Console.WriteLine("Please enter a valid input");
                }
            }
            return output;
        }

        //static void Main (string[] args)
        //{
        //    string input = Console.ReadLine();
        //    double userDouble = InputDoubleParseHandling(input);
        //    Console.WriteLine(userDouble);
        //}
    }
}
