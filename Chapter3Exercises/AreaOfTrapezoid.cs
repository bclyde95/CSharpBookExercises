﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter3Exercises
{
    class AreaOfTrapezoid
    {
        public static double AofTrapezoid(double a, double b, double h)
        {
            double area = (a + b) * h / 2;
            return area;
        }
        //static void Main(string[] args)
        //{
        //    Console.WriteLine(AofTrapezoid(3.0, 5.0, 2.0));
        //}
    }
}
