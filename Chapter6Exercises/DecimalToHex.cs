﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter6Exercises
{
    class DecimalToHex
    {
        static string HexadecimalClassifier(int s)
        {
            string output;
            if (s == 10)
            {
                output = "A";
            }
            else if (s == 11)
            {
                output = "B";
            }
            else if(s == 12)
            {
                output = "C";
            }
            else if (s == 13)
            {
                output = "D";
            }
            else if (s == 14)
            {
                output = "E";
            }
            else if (s == 15)
            {
                output = "E";
            }
            else if (s == 16)
            {
                output = "10";
            }
            else
            {
                output = s.ToString();
            }

            return output;
        }

        static string RemainderOfDivision(int n)
        {
            
            string hex = "";
            int i = 1;
            while (true)
            {
                hex += HexadecimalClassifier(n % 16);  
                
                if (n == 1)
                {
                    break;
                }
                n /= 16;
            }
            
            return InputParseHandling.stringReverser(hex);
        }

        //static void Main(string[] args)
        //{
        //    int n = InputParseHandling.IntParse();
        //    //Console.WriteLine("{0:X}", n);
        //    Console.WriteLine(RemainderOfDivision(n));

        //}
    }
}
