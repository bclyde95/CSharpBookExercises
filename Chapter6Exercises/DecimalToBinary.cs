﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter6Exercises
{
    class DecimalToBinary
    {
        static int[] PowersOfTwo()
        {
            int[] powers = new int[21];
            
            for (int i = powers.Length - 1; i >= 0; i++)
            {
                powers[i] = (int) Math.Pow(2, i);
            }

            return powers;
        }

        static string DecendingPowersOfTwo (int n)
        {
            int[] powersOfTwo = PowersOfTwo();
            string binary = "";

            foreach (int i in powersOfTwo)
            {
                if (i <= n)
                {
                    binary += "1";
                    n -= i;
                }
                else
                {
                    binary += "0";
                }
            }
            return binary;
        }

        static string RemainderOfDivision (int n)
        {
            string binary = "";
            int i = 1;
            while (true)
            {
                if (n % 2 == 1)
                {
                    binary += "1";
                }
                if (n % 2 == 0)
                {
                    binary += "0";
                }
                if (n == 1)
                {
                    break;
                }
                n /= 2;
            }
            
            return InputParseHandling.stringReverser(binary);
        }

        //static void Main(string[] args)
        //{
        //    int input = InputParseHandling.IntParse();
        //    Console.WriteLine(DecendingPowersOfTwo(input));
        //    Console.WriteLine(RemainderOfDivision(input));
        //}
    }
}
