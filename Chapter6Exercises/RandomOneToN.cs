﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter6Exercises
{
    class RandomOneToN
    {
        static void PrintArray(int[] array)
        {
            foreach (int a in array)
            {
                Console.WriteLine(a);
            }
        }

        static int[] RandomNums (int a)
        {
            int[] nums = new int[a];
            int num;
            int i = 0;
            Random random = new Random();
            while (i < a)
            {
                num = random.Next(1, a + 1);
                if (!nums.Contains(num))
                {
                    nums[i] = num;
                }
                if (nums[i] == 0)
                {
                    nums[i] = random.Next(1, a + 1);
                }
                i++;
            }

            return nums;
        }

        //static void Main(string[] args)
        //{
        //    while (true)
        //    {
        //        #region Input

        //        Console.WriteLine("Enter a number to generate random numbers from 1 to your number:");
        //        int userInt = InputParseHandling.IntParse();

        //        #endregion

        //        Console.WriteLine();
        //        PrintArray(RandomNums(userInt));
        //        Console.WriteLine();
        //    }
        //}
    }
}
