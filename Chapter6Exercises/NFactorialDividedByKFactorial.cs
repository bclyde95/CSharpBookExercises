﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter6Exercises
{
    class NFactorialDividedByKFactorial
    {
        static decimal factorialR(int a)
        {
            if (a == 1)
            {
                return 1;
            }
            else
            {
                return factorialR(a - 1) * a;
            }
        }

        static decimal MultiplicationSolution (int a, int b)
        {
            decimal result = 1;
            for (int i = b + 1; i <= a; i++)
            {
                result *= i;
            }
            return result;
        }

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Enter two variables to see their factorials divided");

        //    #region Input

        //    int n = InputParseHandling.IntParse();

        //    int k = InputParseHandling.IntParse();

        //    #endregion

        //    decimal factorialN = factorialR(n);
        //    decimal factorialK = factorialR(k);

        //    decimal divisionR = factorialN / factorialK;

        //    Console.WriteLine(divisionR);

        //    Console.WriteLine(MultiplicationSolution(n, k));
        //}
    }
}
