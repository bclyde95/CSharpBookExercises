﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter6Exercises
{
    class HexToDecimal
    {
        static int HexadecimalClassifier(char s)
        {
            int output;
            if (s == 'A')
            {
                output = 10;
            }
            else if (s == 'B')
            {
                output = 11;
            }
            else if (s == 'C')
            {
                output = 12;
            }
            else if (s == 'D')
            {
                output = 13;
            }
            else if (s == 'E')
            {
                output = 14;
            }
            else if (s == 'F')
            {
                output = 15;
            }
            else if (s == 10)
            {
                output = 16;
            }
            else if (s == '1')
            {
                output = 1;
            }
            else if (s == '2')
            {
                output = 2;
            }
            else if (s == '3')
            {
                output = 3;
            }
            else if (s == '4')
            {
                output = 4;
            }
            else if (s == '5')
            {
                output = 5;
            }
            else if (s == '6')
            {
                output = 6;
            }
            else if (s == '7')
            {
                output = 7;
            }
            else if (s == '8')
            {
                output = 8;
            }
            else if (s == '9')
            {
                output = 9;
            }
            else if (s == '0')
            {
                output = 0;
            }
            else
            {
                output = s;
            }

            return output;
        }

        //static void Main(string[] args)
        //{
        //    int[] multiplesOfSixteen = { 1, 16, 256, 4096, 65536, 1048576 };
        //    int multiple = 1;
        //    string hex = "10";
        //    int decimalNum = 0;
        //    int j = 0;
        //    for (int i = hex.Length - 1; i >= 0; i--)
        //    {
        //        int hexConverted = HexadecimalClassifier(hex[i]);
        //        decimalNum += (HexadecimalClassifier(hex[i]) * multiplesOfSixteen[j]);
        //        j++;
        //    }
        //    Console.WriteLine(decimalNum);
        //}
    }
}
