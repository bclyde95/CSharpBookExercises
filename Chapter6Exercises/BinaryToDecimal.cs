﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Chapter6Exercises
{
    class BinaryToDecimal
    {
        static int BinaryToDecimalConversion(string b)
        {
            int decimalNum = 0;
            for (int i = 0; i < b.Length; i++)
            {
                if (b[b.Length - i - 1] == '0')
                {
                    continue;
                }
                decimalNum += (int)Math.Pow(2, i);
            }
            return decimalNum;
        }

        //static void Main(string[] args)
        //{
        //    while (true)
        //    {
        //        Console.WriteLine("Enter a binary in this format '0000000000'");

        //        string binary = "";
        //        while (true)
        //        {
        //            binary = Console.ReadLine();
        //            if (Regex.IsMatch(binary, "[0-1][0-1][0-1][0-1][0-1][0-1][0-1][0-1][0-1][0-1]"))
        //            {
        //                break;
        //            }
        //            else
        //            {
        //                Console.WriteLine("Must be in format '0000010100'");
        //            }
        //        }

        //        Console.WriteLine(BinaryToDecimalConversion(binary));
        //    }
        //}
    }
}
