﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter6Exercises
{
    class SumOfNandXinFormula
    {
        static decimal Factorial (int a)
        {
            if (a == 1)
            {
                return 1;
            }
            else
            {
                return Factorial(a - 1) * a;
            }
        }

        static decimal DivisionSolution (int a, int b)
        {
            decimal sum = 1;
            decimal factorial;
            int power = 0;

            for (int i = 1; i <= a; i++)
            {
                factorial = Factorial(i);
                power = (int)Math.Pow(b, i);
                sum += (factorial / power);
            }
            return sum;
        }

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Enter n and x");
        //    Console.WriteLine("n:");
        //    int n = InputParseHandling.IntParse();
        //    Console.WriteLine("x:");
        //    int x = InputParseHandling.IntParse();

        //    Console.WriteLine(DivisionSolution(n, x));
        //}
    }
}
