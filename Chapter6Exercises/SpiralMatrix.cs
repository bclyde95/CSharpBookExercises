﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter6Exercises
{
    class SpiralMatrix
    {
        static int[,] Spiral (int a)
        {
            int[,] matrix = new int[a, a];
            int x = 0;
            int y = 0;
            int matrixSize = a;
            int b = 1;

            while (matrixSize > 0)
            {
                for (int i = y; i <= y + matrixSize - 1; i++)
                {
                    matrix[x, i] = b++;
                }

                for (int j = x + 1; j <= x + matrixSize - 1; j++)
                {
                    matrix[j, y + matrixSize - 1] = b++;
                }

                for (int i = y + matrixSize - 2; i >= y; i--)
                {
                    matrix[x + matrixSize - 1, i] = b++;
                }

                for (int i = x + matrixSize - 2; i >= x + 1; i--)
                {
                    matrix[i, y] = b++;
                }

                x = x + 1;
                y = y + 1;
                matrixSize = matrixSize - 2;
            }

            return matrix;
        }

        static void PrintSpiral (int [,] matrix)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {
                    if (matrix[row, col] > 9 && matrix[row, col] < 100)
                    {
                        Console.Write("|  {0} |", matrix[row, col]);
                    }
                    else if (matrix[row, col] > 99)
                    {
                        Console.Write("| {0} |", matrix[row, col]);
                    }
                    else
                    {
                        Console.Write("|  {0}  |", matrix[row, col]);
                    }
                }
                Console.WriteLine();
            }
        }

        //static void Main(string[] args)
        //{
        //    while (true)
        //    {
        //        #region Input

        //        Console.WriteLine("Enter a number to see a spiral matrix from 1 to n^2");
        //        int input = InputParseHandling.IntParse();

        //        #endregion

        //        PrintSpiral(Spiral(input));
        //        Console.WriteLine();
        //    }
        //}
    }
}
