﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;
using System.Numerics;

namespace Chapter6Exercises
{
    class NumberOfZerosAtEndOfFactorial
    {
        static string NumOfZeros (int a)
        {
            BigInteger factorial = 1;
            int count = 0;

            while (true)
            {
                if (a == 1)
                {
                    break;
                }
                if (a % 5 == 0)
                {
                    count += 1;
                }
                factorial *= a;
                a--;
            }

            return string.Format("{0} contains {1} zeros", factorial, count);
        }

        //static void Main (string[] args)
        //{
        //    while (true)
        //    {

        //        #region Input

        //        Console.WriteLine("Enter a number between 1 and 1000 to see how many zeros it's Factorial ends with:");

        //        int n;
        //        while (true)
        //        {
        //            n = InputParseHandling.IntParse();
        //            if (n > 0 && n <= 1000)
        //            {
        //                break;
        //            }
        //            else
        //            {
        //                Console.WriteLine("Number must be between 1 and 1000");
        //            }
        //        }

        //        #endregion

        //        Console.WriteLine(NumOfZeros(n));

        //    }
        //}
    }
}
