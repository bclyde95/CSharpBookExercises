﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter6Exercises
{
    class GCDandLCM
    {
        static int GCD(int a, int b)
        {
            if (a == 0)
            {
                return b;
            }
            return GCD(b % a, a);
        }

        static int LCM(int a, int b) => Math.Abs(a * b) / GCD(a, b);

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Enter two numbers to find there GCD & LCM");
        //    Console.WriteLine("a:");
        //    int i = InputParseHandling.IntParse();
        //    Console.WriteLine("b:");
        //    int j = InputParseHandling.IntParse();

        //    Console.WriteLine(GCD(i, j));
        //    Console.WriteLine(LCM(i, j));
        //}
    }
}
