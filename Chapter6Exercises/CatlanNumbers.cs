﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter6Exercises
{
    class CatlanNumbers
    {
        static List<int> FactorialToArrayNum (int a)
        {
            List<int> aFactorial = new List<int>();

            for (int i = (a * 2); i > a; i--)
            {
                aFactorial.Add(i);
            }

            return aFactorial;
        }

        static List<int> FactorialToArrayDen(int a)
        {
            List<int> aFactorial = new List<int>();

            for (int i = (a + 1); i > 0; i--)
            {
                aFactorial.Add(i);
            }

            return aFactorial;
        }

        static decimal factorial (int a)
        {
            if (a == 1)
            {
                return 1;
            }
            else
            {
                return factorial(a - 1) * a;
            }
        }

        static decimal SimplifyFactorials(int a)
        {
            try
            {
                List<int> bArray = FactorialToArrayNum(a);
                List<int> aArray = FactorialToArrayDen(a); 
                for (int i = 0; i < aArray.Count; i++)
                {
                    foreach (int j in bArray)
                    {
                        if (aArray[i] == j)
                        {
                            aArray[i] = 1;
                        }
                    }
                }
                decimal product = 1;
                foreach (int k in aArray)
                {
                    product *= k;
                }
                return product;
            }
            catch (DivideByZeroException)
            {
                return 0;
            }
        }

        static decimal DivisionSoulution(int a)
        {
            decimal result = factorial(a * 2) / (factorial(a + 1) * factorial(a));
            return result;
        }

        static decimal RecursiveCatalan(int a)
        {
            if (a == 0)
            {
                return 1;
            }
            else
            {
                int formula = (2 * (2 * a - 1)) / (a + 1);
                return formula * RecursiveCatalan(a - 1);
            }
        }

        //static void Main (string[] args)
        //{
        //    Console.WriteLine("Enter n to find the nth Catalan Number");
        //    int n = InputParseHandling.IntParse();

        //    Console.WriteLine(SimplifyFactorials(n));
        //    foreach(int l in FactorialToArrayNum(n))
        //    {
        //        Console.WriteLine(l);
        //    }
        //    foreach(int k in FactorialToArrayDen(n))
        //    {
        //        Console.WriteLine(k);
        //    }
        //    Console.WriteLine(DivisionSoulution(n));
        //    Console.WriteLine(RecursiveCatalan(n));
        //}
    }
}
