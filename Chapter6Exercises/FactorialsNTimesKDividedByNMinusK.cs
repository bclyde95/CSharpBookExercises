﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;
using System.Numerics;

namespace Chapter6Exercises
{
    class FactorialsNTimesKDividedByNMinusK
    {
        static List<int> FactorialToArrayNum (int a)
        {
            List<int> aFactorial = new List<int>();
            

            for (int i = a; i > 0; i--)
            {
                aFactorial.Add(i);
            }

            return aFactorial;
        }

        static List<int> FactorialToArrayDen(int a, int b)
        {
            List<int> aFactorial = new List<int>();

            for (int i = (a-b); i > b; i--)
            {
                aFactorial.Add(i);
            }

            return aFactorial;
        }

        static decimal factorial (int a)
        {
            if(a == 1)
            {
                return 1;
            }
            else
            {
                return factorial(a - 1) * a;
            }
        }

        static decimal SimplifyFactorials(int a, int b)
        {
            try
            {
                List<int> aArray = FactorialToArrayNum(a);
                List<int> bArray = FactorialToArrayDen(a, b);
                int l = 0;
                for (int i = 0; i < aArray.Count; i++)
                {
                    foreach (int j in bArray)
                    {
                        if (aArray[i] == j)
                        {
                            aArray[i] = 1;
                        }
                    }
                }
                decimal product = 1;
                foreach(int k in aArray)
                {
                    product *= k;
                }
                return product;
            }
            catch (DivideByZeroException)
            {
                return 0;
            }
        }

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("(N!*K!) / (N!-K!)");

        //    Console.WriteLine("Enter N:");
        //    int n = InputParseHandling.IntParse();
        //    Console.WriteLine("Enter K:");
        //    int k = InputParseHandling.IntParse();
        //    Console.WriteLine("");

        //    decimal factorialN = factorial(n);
        //    decimal factorialK = factorial(k);
        //    decimal result = 0;

        //    try
        //    {
        //        result = (factorialN * factorialK) / (factorial(n - k));
        //    }
        //    catch (DivideByZeroException)
        //    {
        //        result = 0;
        //    }

        //    Console.WriteLine(result);
        //    Console.WriteLine(SimplifyFactorials(n, k));
        //}
    }
}