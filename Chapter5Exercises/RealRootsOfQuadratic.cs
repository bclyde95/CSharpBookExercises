﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter5Exercises
{
    class RealRootsOfQuadratic
    {
        static void realRoot (int a, int b, int c)
        {
            double discriminant = b * b - (4 * a * c);
            double x1;
            double x2;
            
            if (discriminant == 0)
            {
                x1 = (b - (b * 2.0)) / (2.0 * a);
                Console.WriteLine(x1);
            } 
            if (discriminant > 0)
            {
                x1 = (b - (b * 2)) + Math.Sqrt((b * b) - (4.0 * a * c)) / (2.0 * a);
                x2 = (b - (b * 2)) - Math.Sqrt((b * b) - (4.0 * a * c)) / (2.0 * a);
                Console.WriteLine("x = {0} and x = {1}", x1, x2);
            }
            if (discriminant < 0)
            {
                Console.WriteLine(0);
            }
        }

        //static void Main (string[] args)
        //{
        //    Console.WriteLine("Enter a, b, and c coefficients for quadratic \nax^2 + bx + c = 0, to find it's real roots");

        //    Console.WriteLine("Enter a:");
        //    int a = InputParseHandling.IntParse();

        //    Console.WriteLine("Enter b:");
        //    int b = InputParseHandling.IntParse();

        //    Console.WriteLine("Enter c:");
        //    int c = InputParseHandling.IntParse();

        //    realRoot(a, b, c);
        //}
    }
}
