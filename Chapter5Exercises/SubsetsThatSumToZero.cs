﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter5Exercises
{
    class SubsetsThatSumToZero
    {
        //static void Main(string[] args)
        //{
        //    // Program Loop
        //    while (true)
        //    {
        //        Console.WriteLine("Enter 5 integers and see the combinations that sum to zero");

        //        #region Input
        //        // User enters 5 numbers and they are parsed to int on the way in
        //        Console.WriteLine("Enter your first number:");
        //        int a = InputParseHandling.IntParse();

        //        Console.WriteLine("Enter your second number:");
        //        int b = InputParseHandling.IntParse();

        //        Console.WriteLine("Enter your third number:");
        //        int c = InputParseHandling.IntParse();

        //        Console.WriteLine("Enter your fourth number:");
        //        int d = InputParseHandling.IntParse();

        //        Console.WriteLine("Enter your fifth number:");
        //        int e = InputParseHandling.IntParse();
        //        #endregion

        //        int[] digits = { a, b, c, d, e };
        //        int start = 0;
        //        int counter = 0;

        //        #region Groups of Two Search
        //        // start + i looped over all possible combinations
        //        while (true)
        //        {
        //            for (int i = 0; i < digits.Count(); i++)
        //            {
        //                if (digits[start] + digits[i] == 0)
        //                {
        //                    if (start != i)
        //                    {
        //                        counter++;
        //                        Console.WriteLine("The sum of: \n{0} (location: {2}) \n{1} (location: {3}) \nis 0.\n", digits[start], digits[i], start, i);
        //                    }
        //                }
        //            }
        //            start += 1;
        //            if (start == digits.Count())
        //            {
        //                break;
        //            }
        //        }
        //        #endregion

        //        #region Groups of Three Search
        //        // start + i + j looped over all possible combinations
        //        start = 0;
        //        while (true)
        //        {
        //            for (int i = 0; i < digits.Count(); i++)
        //            {
        //                for (int j = 1; j < digits.Count(); j++)
        //                {
        //                    if (digits[start] + digits[i] + digits[j] == 0)
        //                    {
        //                        bool original3 = start != i && start != j && i != j;
        //                        if (original3)
        //                        {
        //                            counter++;
        //                            Console.WriteLine("The sum of: \n{0} (location: {3}), \n{1} (location: {4}), \n{2} (location: {5}) \nis 0.\n", digits[start], digits[i], digits[j], start, i, j);
        //                        }
        //                    }
        //                }
        //            }
        //            start += 1;
        //            if (start == digits.Count())
        //            {
        //                break;
        //            }
        //        }
        //        #endregion

        //        #region Groups of Four Search
        //        // start + i + j + k looped over all possible combinations
        //        start = 0;
        //        while (true)
        //        {
        //            for (int i = 0; i < digits.Count(); i++)
        //            {
        //                for (int j = 1; j < digits.Count(); j++)
        //                {
        //                    for (int k = 2; k < digits.Count(); k++)
        //                    {
        //                        if (digits[start] + digits[i] + digits[j] + digits[k] == 0)
        //                        {
        //                            bool original4 = start != i && start != j && start != k && i != j && i != k && j != k;
        //                            if (original4)
        //                            {
        //                                counter++;
        //                                Console.WriteLine("The sum of: \n{0} (location: {4}), \n{1} (location: {5}), \n{2} (location: {5}), \n{3} (location: {7}) \nis 0.\n", digits[start], digits[i], digits[j], digits[k], start, i, j, k);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            start += 1;
        //            if (start == digits.Count())
        //            {
        //                break;
        //            }
        //        }
        //        #endregion

        //        #region Sum of 5
        //        // Handles the possibility that the sum of all 5 numbers equals 0
        //        if (digits.Sum() == 0)
        //        {
        //            counter++;
        //            Console.WriteLine("The sum of all integers is 0\n");
        //        }
        //        #endregion

        //        // Counter keeps track of how many equal zero, if any
        //        if (counter == 0)
        //        {
        //            Console.WriteLine("\nNo combinations equal zero");
        //        }
        //        else
        //        {
        //            Console.WriteLine("\nThe total # of combinations that equal zero are: {0}", counter);
        //        }
        //        Console.WriteLine("");
        //        Console.WriteLine("");
        //        Console.WriteLine("");
        //    }
        //}
    }
}
