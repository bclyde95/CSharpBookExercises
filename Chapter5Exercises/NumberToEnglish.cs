﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomFuncs;

namespace Chapter5Exercises
{
    class NumberToEnglish
    {
        static int hundredsClassifier (int input)
        {
            int hundreds = 0;
            if (input >= 100 && input <= 999)
            {
                if (input >= 100 && input <= 199)
                {
                    hundreds = 100;
                }
                else if (input >= 200 && input <= 299)
                {
                    hundreds = 200;
                }
                else if (input >= 300 && input <= 399)
                {
                    hundreds = 300;
                }
                else if (input >= 400 && input <= 499)
                {
                    hundreds = 400;
                }
                else if (input >= 500 && input <= 599)
                {
                    hundreds = 500;
                }
                else if (input >= 600 && input <= 699)
                {
                    hundreds = 600;
                }
                else if (input >= 700 && input <= 799)
                {
                    hundreds = 700;
                }
                else if (input >= 800 && input <= 899)
                {
                    hundreds = 800;
                }
                else if (input >= 900 && input <= 999)
                {
                    hundreds = 900;
                }
            }
            return hundreds;
        }

        static int tenClassifier (int input)
        {
            int tens = 0;
            if (input >= 20 && input <= 99)
            {
                if (input >= 20 && input <= 29)
                {
                    tens = 20;
                }
                else if (input >= 30 && input <= 39)
                {
                    tens = 30;
                }
                else if (input >= 40 && input <= 49)
                {
                    tens = 40;
                }
                else if (input >= 50 && input <= 59)
                {
                    tens = 50;
                }
                else if (input >= 60 && input <= 69)
                {
                    tens = 60;
                }
                else if (input >= 70 && input <= 79)
                {
                    tens = 70;
                }
                else if (input >= 80 && input <= 89)
                {
                    tens = 80;
                }
                else if (input >= 90 && input <= 99)
                {
                    tens = 90;
                }
            }
            return tens;
        }

        static string moreThanTen(int input, IDictionary<int, string> twentyToNinety, IDictionary<int, string> oneToTen)
        {
            int ten = tenClassifier(input);
            if (input >= ten && input <= ten + 9)
            {
                if (input == ten)
                {
                    return twentyToNinety[input];
                }
                else
                {
                    int secondnumber = input - ten;
                    return string.Format("{0} {1}", twentyToNinety[ten], oneToTen[secondnumber]);
                }
            }
            
            return "Error";
        }

        static string moreThanOneHundred(int input, IDictionary<int, string> hundreds, IDictionary<int, string> twentyToNinety, IDictionary<int, string> elevenToNineteen, IDictionary<int, string> oneToTen)
        {
            int hundred = hundredsClassifier(input);
            if (input >= hundred && input <= hundred + 99)
            {
                if (input == hundred)
                {
                    return hundreds[input];
                }
                else
                {
                    if (input >= hundred + 20)
                    {
                        int tensNumbers = input - hundred;
                        return string.Format("{0} {1}", hundreds[hundred], moreThanTen(tensNumbers, twentyToNinety, oneToTen));
                    }
                    if (input >= hundred + 11 && input <= hundred + 19)
                    {
                        int teensNumber = input - hundred;
                        return string.Format("{0} {1}", hundreds[hundred], elevenToNineteen[teensNumber]);
                    }
                    if (input >= hundred + 1 && input <= hundred + 10) 
                    {
                        int onesNumber = input - hundred;
                        return string.Format("{0} and {1}", hundreds[hundred], oneToTen[onesNumber]);
                    }
                }        
            }
            return "Error";
        }


        //static void Main(string[] args)
        //{
        //    while (true)
        //    {
        //        #region Zero - Ten Dictionary

        //        IDictionary<int, string> oneThroughTen = new Dictionary<int, string>();
        //        oneThroughTen.Add(1, "One");
        //        oneThroughTen.Add(2, "Two");
        //        oneThroughTen.Add(3, "Three");
        //        oneThroughTen.Add(4, "Four");
        //        oneThroughTen.Add(5, "Five");
        //        oneThroughTen.Add(6, "Six");
        //        oneThroughTen.Add(7, "Seven");
        //        oneThroughTen.Add(8, "Eight");
        //        oneThroughTen.Add(9, "Nine");
        //        oneThroughTen.Add(10, "Ten");

        //        #endregion

        //        #region Eleven - Nineteen Dictionary

        //        IDictionary<int, string> elevenThroughNineteen = new Dictionary<int, string>();
        //        elevenThroughNineteen.Add(11, "Eleven");
        //        elevenThroughNineteen.Add(12, "Twelve");
        //        elevenThroughNineteen.Add(13, "Thirteen");
        //        elevenThroughNineteen.Add(14, "Fourteen");
        //        elevenThroughNineteen.Add(15, "Fifteen");
        //        elevenThroughNineteen.Add(16, "Sixteen");
        //        elevenThroughNineteen.Add(17, "Seventeen");
        //        elevenThroughNineteen.Add(18, "Eighteen");
        //        elevenThroughNineteen.Add(19, "Nineteen");

        //        #endregion

        //        #region Twenty - Ninety Keyword Dictionary

        //        IDictionary<int, string> multiplesOfTen = new Dictionary<int, string>();
        //        multiplesOfTen.Add(20, "Twenty");
        //        multiplesOfTen.Add(30, "Thirty");
        //        multiplesOfTen.Add(40, "Forty");
        //        multiplesOfTen.Add(50, "Fifty");
        //        multiplesOfTen.Add(60, "Sixty");
        //        multiplesOfTen.Add(70, "Seventy");
        //        multiplesOfTen.Add(80, "Eighty");
        //        multiplesOfTen.Add(90, "Ninety");

        //        #endregion

        //        #region Hundreds Keyword Dictionary

        //        IDictionary<int, string> multiplesOfOneHundred = new Dictionary<int, string>();
        //        multiplesOfOneHundred.Add(100, "One Hundred");
        //        multiplesOfOneHundred.Add(200, "Two Hundred");
        //        multiplesOfOneHundred.Add(300, "Three Hundred");
        //        multiplesOfOneHundred.Add(400, "Four Hundred");
        //        multiplesOfOneHundred.Add(500, "Five Hundred");
        //        multiplesOfOneHundred.Add(600, "Six Hundred");
        //        multiplesOfOneHundred.Add(700, "Seven Hundred");
        //        multiplesOfOneHundred.Add(800, "Eight Hundred");
        //        multiplesOfOneHundred.Add(900, "Nine Hundred");

        //        #endregion

        //        //Title statement
        //        Console.WriteLine("Enter a number from 0 to 1000 to see it's corresponding word:");

        //        #region Input

        //        int userInt;
        //        while (true)
        //        {
        //            userInt = InputParseHandling.IntParse();
        //            if (userInt >= 0 && userInt <= 1000)
        //            {
        //                break;
        //            }
        //            else
        //            {
        //                if (userInt < 0)
        //                {
        //                    Console.WriteLine("Number must be greater than 0");
        //                }
        //                else
        //                {
        //                    Console.WriteLine("Number must be less than 1001");
        //                }
        //            }
        //        }

        //        #endregion

        //        Console.WriteLine("");

        //        #region Writing to Console

        //        if (userInt == 0)
        //        {
        //            Console.WriteLine(">>> Zero");
        //        }
        //        else if (userInt >= 1 && userInt <= 10)
        //        {
        //            Console.WriteLine(">>> " + oneThroughTen[userInt]);
        //        }
        //        else if (userInt >= 11 && userInt <= 19)
        //        {
        //            Console.WriteLine(">>> " + elevenThroughNineteen[userInt]);
        //        }
        //        else if (userInt >= 20 && userInt <= 99)
        //        {
        //            Console.WriteLine(">>> " + moreThanTen(userInt, multiplesOfTen, oneThroughTen));
        //        }
        //        else if (userInt >= 100 && userInt <= 999)
        //        {
        //            Console.WriteLine(">>> " + moreThanOneHundred(userInt, multiplesOfOneHundred, multiplesOfTen, elevenThroughNineteen, oneThroughTen));
        //        }
        //        else if (userInt == 1000)
        //        {
        //            Console.WriteLine(">>> " + "One Thousand");
        //        }

        //        #endregion

        //        Console.WriteLine("");
        //        Console.WriteLine("");
        //    }
        //}
    }
}
