﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter5Exercises
{
    class PositiveOrNegativeWithoutCalculation
    {
        static int negativeCount(int n, int o, int p)
        {
            int count = 0;
            if (n < 0)
                count += 1;
            if (o < 0)
                count += 1;
            if (p < 0)
                count += 1;
            return count;
        }

        //static void Main (string[] args)
        //{
        //    int a = 4;
        //    int b = 8;
        //    int c = 5;
        //    int negCount = negativeCount(a, b, c);

        //    if (negCount % 2 == 0)
        //    {
        //        Console.WriteLine("Positive");
        //    }
        //    else
        //    {
        //        Console.WriteLine("Negative");
        //    }

        //}
    }
}
